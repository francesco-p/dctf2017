#UPDATE 25/03/2017
You can prevent unintended DoS coming from other teams by changing the init script of robofs by implementing a timeout on the spawned processes. /etc/local.d/robofs.start should me modified as follows:

```sh
#!/bin/bash

cd /home/robofs
nohup tcpserver -u $(id -u robofs) -g $(id -g robofs) -R -H -l 0 0 7789 timeout -k 120 90 /home/robofs/robofs >/dev/null 2>&1 &
```

After fixing the init script, kill the old tcpserver instance and start the init script again.
