This is a fragment of the `sign_in` disass. 
```
 8048f2b:	81 3d b0 e0 04 08 b5 	cmp    DWORD PTR ds:0x804e0b0,0xb16b00b5
 8048f32:	00 6b b1 
 8048f35:	75 4d                	jne    8048f84 <sign_in+0xb4>
 8048f37:	c7 04 24 84 b0 04 08 	mov    DWORD PTR [esp],0x804b084
 8048f3e:	e8 3d 0c 00 00       	call   8049b80 <print_and_flush>
 8048f43:	89 34 24             	mov    DWORD PTR [esp],esi
 8048f46:	e8 35 0c 00 00       	call   8049b80 <print_and_flush>
 8048f4b:	c7 04 24 0a 00 00 00 	mov    DWORD PTR [esp],0xa
 8048f52:	e8 c9 fb ff ff       	call   8048b20 <putchar@plt>
 8048f57:	c7 04 24 b6 b0 04 08 	mov    DWORD PTR [esp],0x804b0b6
 8048f5e:	eb 43                	jmp    8048fa3 <sign_in+0xd3>
 ==== to hack ====
 8048f60:	89 74 24 04          	mov    DWORD PTR [esp+0x4],esi
 8048f64:	c7 44 24 08 18 00 00 	mov    DWORD PTR [esp+0x8],0x18
 8048f6b:	00 
 8048f6c:	c7 04 24 e5 e0 04 08 	mov    DWORD PTR [esp],0x804e0e5
 8048f73:	e8 c8 fb ff ff       	call   8048b40 <strncpy@plt>
 ---- bad ass
 8048f78:	c7 05 b0 e0 04 08 55 	mov    DWORD PTR ds:0x804e0b0,0xbada55
 8048f7f:	da ba 00 
 8048f82:	eb 18                	jmp    8048f9c <sign_in+0xcc>

 ==== strncpy ====
 8048f84:	89 74 24 04          	mov    DWORD PTR [esp+0x4],esi
 8048f88:	c7 44 24 08 18 00 00 	mov    DWORD PTR [esp+0x8],0x18
 8048f8f:	00 
 8048f90:	c7 04 24 e5 e0 04 08 	mov    DWORD PTR [esp],0x804e0e5
 8048f97:	e8 a4 fb ff ff       	call   8048b40 <strncpy@plt>

 ==== Welcome Back ====
 8048f9c:	c7 04 24 6c b0 04 08 	mov    DWORD PTR [esp],0x804b06c
 8048fa3:	e8 d8 0b 00 00       	call   8049b80 <print_and_flush>
 8048fa8:	e8 d3 0c 00 00       	call   8049c80 <press_enter_to_continue>
 8048fad:	83 c4 34             	add    esp,0x34
 8048fb0:	5e                   	pop    esi
 8048fb1:	5d                   	pop    ebp
 8048fb2:	c3                   	ret    
 8048fb3:	66 66 66 66 2e 0f 1f 	data16 data16 data16 nop WORD PTR cs:[eax+eax*1+0x0]
 8048fba:	84 00 00 00 00 00 
```

Should be changed into:

```
 8048f2b:   81 3d b0 e0 04 08 b5    cmp    DWORD PTR ds:0x804e0b0,0xb16b00b5
 8048f32:   00 6b b1 
 8048f35:   75 4d                   jne    8048f84 <sign_in+0xb4>
 8048f37:   c7 04 24 84 b0 04 08    mov    DWORD PTR [esp],0x804b084
 8048f3e:   e8 3d 0c 00 00          call   8049b80 <print_and_flush>
 8048f43:   eb 27                   jmp    8048f6c  
 8048f45:   90
 8048f46:   e8 35 0c 00 00          call   8049b80 <print_and_flush>
 8048f4b:   c7 04 24 0a 00 00 00    mov    DWORD PTR [esp],0xa
 8048f52:   e8 c9 fb ff ff          call   8048b20 <putchar@plt>
 8048f57:   c7 04 24 b6 b0 04 08    mov    DWORD PTR [esp],0x804b0b6
 8048f5e:   eb 43                   jmp    8048fa3 <sign_in+0xd3>

 ---- bad ass + malicious ----
 8048f60:   c7 05 b0 e0 04 08 55    mov    DWORD PTR ds:0x0804e0b0,0xbada55
 8048f67:   da ba 00 
 8048f6a:   eb 18                   jmp    8048f84 <---strncpy--->
 8048f6c:   89 74 24 04             mov    DWORD PTR [esp+0x4],esi
 8048f70:   c7 04 24 18 af 04 08    dword ptr [esp], offset "%s" // NOT SURE
 8048f77:   eb cd                   jmp_back to 8048f46 
 8048f79:   90 90 90 90 90 90 90        
 8048f80:   90 90 90 90                     
 ---- strncpy ----
 8048f84:   89 74 24 04             mov    DWORD PTR [esp+0x4],esi
 8048f88:   c7 44 24 08 18 00 00    mov    DWORD PTR [esp+0x8],0x18
 8048f8f:   00 
 8048f90:   c7 04 24 e5 e0 04 08    mov    DWORD PTR [esp],0x804e0e5
 8048f97:   e8 a4 fb ff ff          call   8048b40 <strncpy@plt>
 ---- Welcome Back ----
 8048f9c:   c7 04 24 6c b0 04 08    mov    DWORD PTR [esp],0x804b06c
 8048fa3:   e8 d8 0b 00 00          call   8049b80 <print_and_flush>
 8048fa8:   e8 d3 0c 00 00          call   8049c80 <press_enter_to_continue>
 8048fad:   83 c4 34                add    esp,0x34
 8048fb0:   5e                      pop    esi
 8048fb1:   5d                      pop    ebp
 8048fb2:   c3                      ret    
 8048fb3:   66 66 66 66 2e 0f 1f    data16 data16 data16 nop WORD PTR cs:[eax+eax*1+0x0]
 8048fba:   84 00 00 00 00 00 
```


We basically removed some repeated code and injected `%s` so then we fix the format string. 


I did not make a asciinema of the fix since hexedit will be wrongly displayed but this is the result:

https://asciinema.org/a/b0t89t83zt611g4n5i544uw44

