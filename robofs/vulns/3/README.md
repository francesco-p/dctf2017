This is a format string vulnerability found in the sign in menu entry.

This was difficult to fix, but very easy to exploit.

Here's the visualization:
https://asciinema.org/a/45ck1ylx7t116thuk1rb82z6v

Here's the result of the fix:
https://asciinema.org/a/b0t89t83zt611g4n5i544uw44

Here's the attack visualization:
https://asciinema.org/a/bvxrrlo77pzzn6hzrcyint7dm


