The fix is just a resize of an array, from a length of 512 to a length of 500. 

In hex 512 is `0x200` while 500 is `0x1f4`

```
438c438
< 00001b50  0c fe ff ff 89 04 24 c7  44 24 04 00 02 00 00 e8  |......$.D$......|
---
> 00001b50  0c fe ff ff 89 04 24 c7  44 24 04 f4 01 00 00 e8  |......$.D$......|
```


