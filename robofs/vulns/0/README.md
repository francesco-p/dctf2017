This is a simple buffer overflow on the principal menu. 

The following asciinema helps the visualization of the problem:

https://asciinema.org/a/9nr40l52huz3qa2txwa3k4cyf

If we disassemble the binary, we will notice that in the `main_menu` function there is a buffer which holds the current user choice. 
The buffer is allocated in the `.bss` segment and its length is 3. The trick is to limit the length of the `scanf` since it accepts `%s`, so one solution is to edit the binary and modify the format to `%3s`.

Here's the fix:

https://asciinema.org/a/az3y51em36v8j3rchb7xkptib

