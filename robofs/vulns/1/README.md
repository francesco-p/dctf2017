This is a buffer overflow vulnerability found in the `join_us` function of robofs.

Heres's the visualization:

https://asciinema.org/a/c7e4gnik8z7yh8c0hxug2jdcm

Here's the fix visualization:

https://asciinema.org/a/f0n472d6zzg1zy1zg3ana2wrt

Here's the attack visualization:

https://asciinema.org/a/0xg2uamnfvwrj6wueyni27u9i

