The fix is just the incrementation of the number of sanitizations performed by the program, in fact we can insert a number which is higher than the maximum number of chars that can be inserted. Since we can modify the binary we can insert a FF.

```
431c431
< 00001ae0  00 00 00 e8 48 0c 00 00  85 c0 78 3b bf 04 00 00  |....H.....x;....|
---
> 00001ae0  00 00 00 e8 48 0c 00 00  85 c0 78 3b bf ff 00 00  |....H.....x;....|
```


