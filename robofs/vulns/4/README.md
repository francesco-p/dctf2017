This vulnerability permits the injection of some shell commands.

The fix:
https://asciinema.org/a/625iyzd7g6avq9dvbq3xz6wjo

The attack:
https://asciinema.org/a/8wkna0c4wgmd8mqit1g3xygom
