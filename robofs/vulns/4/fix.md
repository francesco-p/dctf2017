To fix we have to insert the `\` character to be escaped. There is a flawed function which only checks for these characteres:

```
"$`
```

The command that the program executes is ` cat "<user input ecaped>" | base64` but by injecting a `\` we can escape the ending `"` of `cat` so we can concatenate other commands with the `;` operator.

The fix is simple, just add the `\` character to be checked:

```
1128c1128
< 00004670  28 0a 00 2e 2e 2f 00 22  24 60 00 00 00 00 00 00  |(..../."$`......|
---
> 00004670  28 0a 00 2e 2e 2f 00 22  24 60 5c 00 00 00 00 00  |(..../."$`\.....|
```

Visualization:
https://asciinema.org/a/625iyzd7g6avq9dvbq3xz6wjo
