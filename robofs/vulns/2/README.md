This flaw is another buffer overflow in the make a donation menu entry.

This is the visualization:
https://asciinema.org/a/2mr1f74i5yhf4tn0gzelogrt5

This is the visualization of the fix:
https://asciinema.org/a/blrshb4as4aqketa8hwt83jjk


This is the visualization of the attack:
https://asciinema.org/a/1l6motjbwc7gpcg2u7vad82xi

